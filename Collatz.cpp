// -----------
// Collatz.c++
// -----------

// --------
// includes
// --------

#include <cassert>  // assert
#include <iostream> // endl, istream, ostream
#include <vector>

#include "Collatz.hpp"

#define CACHE_SIZE (1000000)

using namespace std;

// Cache of cycle length of n (index by n-1)
long int cache[CACHE_SIZE] = {-1};
// Flag to track if the cache has been initialized
bool cache_init;

// Initialize cache to all -1
void init_cache() {
    if (!cache_init) {
        for (int i = 0; i < CACHE_SIZE; i++) {
            cache[i] = -1;
        }
        cache_init = true;
    }
}

// Convenience print method for debugging
void print_cache() {
    for(int i = 0; i < CACHE_SIZE; i++) {
        if (cache[i] != -1) {
            cout << "cache[" << i+1 << "]: " << cache[i] << endl;
        }
    }
}

// Populate cache with history from a run of cycle_length
void update_cache(vector<long int> &history, int count) {
    for (int i = 0; i < (int)history.size(); i++) {
        if (history[history.size()-i-1] <= CACHE_SIZE) {
            cache[history[history.size()-i-1]-1] = count + i;
        }
    }
}

// ------------
// collatz_read
// ------------

istream& collatz_read (istream& r, int& i, int& j) {
    return r >> i >> j;
}


// Return the collatz cycle length of a given n
int cycle_length (long int n) {
    int count = 1;
    // History stores the current path walked by the while loop,
    // and is used to populate the path.
    vector<long int> history;
    while (n != 1) {
        history.push_back(n);
        if (n <= CACHE_SIZE && cache[n-1] != -1) {
            update_cache(history, cache[n-1]);
            return cache[n-1] + count;
        }
        // If n is even, (n/2). Otherwise, (3n+1).
        if (n % 2 == 0) {
            n = n / 2;
        } else {
            n = n * 3 + 1;
        }
        count ++;
    }
    update_cache(history, 1);
    return count;
}

// ------------
// collatz_eval
// ------------

int collatz_eval (int i, int j) {
    init_cache();
    int max = 0;
    int m = j/2+1;
    if (m > i) {
        i = m;
    }
    for (int n = i; n <= j; n++) {
        int length = cycle_length(n);
        max = length > max ? length : max;
    }
    return max;
}

// -------------
// collatz_print
// -------------

void collatz_print (ostream& w, int i, int j, int v) {
    w << i << " " << j << " " << v << endl;
}

// -------------
// collatz_solve
// -------------

void collatz_solve (istream& r, ostream& w) {
    init_cache();
    int i;
    int j;
    while (collatz_read(r, i, j)) {
        int v;
        if (i > j) {
            v = collatz_eval(j, i);
        } else {
            v = collatz_eval(i, j);
        }
        collatz_print(w, i, j, v);
    }
}
